package com.tymofieiev.sergii.mobox;


import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by Sergii Tymofieiev on 27.01.2017.
 */

public class MessageDialog extends DialogFragment implements View.OnClickListener {

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_time, container, true);
        ImageView imageView = (ImageView) view.findViewById(R.id.dsi_cover);
        TextView textView = (TextView) view.findViewById(R.id.dsi_header_1);
        view.findViewById(R.id.button_container).setOnClickListener(this);
        getDialog().getWindow().setBackgroundDrawableResource(R.color.black_trans_30);
        getDialog().requestWindowFeature(STYLE_NO_TITLE);

        setCancelable(false);

        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();

        ImageLoader.getInstance().displayImage("http://trinixy.ru/pics5/20170126/decoration_coins_02.jpg", imageView, options, new AnimateFirstDisplayListener());
        textView.setText(String.format(Utils.getStringById(App.getContext(), R.string.current_time), Utils.getFormattedDateAsString(Utils.DateFormatCustom.HHmm, System.currentTimeMillis())));

        return view;
    }

    @Override
    public void onClick(View view) {
        dismiss();

    }
}
