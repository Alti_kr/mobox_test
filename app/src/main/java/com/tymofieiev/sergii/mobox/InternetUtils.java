package com.tymofieiev.sergii.mobox;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Sergii Tymofieiev on 26.01.2017.
 */

public class InternetUtils {
    public enum InternetStatus {
        NO_INTERNET,
        HAVE_GPRS,
        HAVE_WIFI,
    }

    public static InternetStatus getInternetStatus(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm == null) {
            return InternetStatus.NO_INTERNET;
        }
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        if (netInfo == null) {
            return InternetStatus.NO_INTERNET;
        }
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI")) // if we have WiFi ,
                // GPRS not active
                if (ni.isConnected()) {
                    return InternetStatus.HAVE_WIFI;
                }
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected()) {
                    return InternetStatus.HAVE_GPRS;
                }
        }
        return InternetStatus.NO_INTERNET;
    }

}
