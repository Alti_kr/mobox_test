package com.tymofieiev.sergii.mobox;

import android.content.Context;
import android.content.Intent;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by Sergii Tymofieiev on 26.01.2017.
 */

public class Utils {
    public static final long SECOND = 1000;
    public static final long MINUTE = SECOND * 60;
    public static final long HOUR = MINUTE * 60;
    public static final long DAY = HOUR * 24;


    public enum DateFormatCustom {
        ddMMyyyy("dd.MM.yyyy"),
        HHmm("HH:mm"),;
        String formatString;

        private DateFormatCustom(String tFString) {
            this.formatString = tFString;
        }
    }

    public static String getStringById(Context context, int id) {
        return context.getResources().getString(id);
    }

    public static String getFormattedDateAsString(DateFormatCustom dateFormatCustom, Long tDate) {
        try {
            return new SimpleDateFormat(dateFormatCustom.formatString).format(new Date(tDate));
        } catch (Exception e) {
            return "";
        }
    }

    public static String safeGetJsonValue(String key, JSONObject jsonObject, String defaultValue) throws JSONException {
        if (jsonObject.isNull(key)) {
            return defaultValue;
        }
        return jsonObject.getString(key);
    }

    public static Long safeParseLong(String value, Long defValue) {
        try {
            return Long.parseLong(value);
        } catch (Exception e) {
            return defValue;
        }
    }

    public static Intent getServicesIntent(Context context) {
        Intent intent = new Intent(context, MessageService.class);
        intent.putExtra(Constants.SERVICE_ACTION_START, true);
        return intent;
    }
}

