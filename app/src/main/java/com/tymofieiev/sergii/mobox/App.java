package com.tymofieiev.sergii.mobox;


import android.app.Application;
import android.content.Context;
import android.os.Handler;

/**
 * Created by Sergii Tymofieiev on 27.01.2017.
 */

public class App extends Application {
    private static App instance;

    public App() {
        instance = this;
    }

    public static Context getContext() {
        return instance.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        startService();
    }

    private static void startService() {
        getContext().startService(Utils.getServicesIntent(getContext()));
        workHandler.sendEmptyMessageDelayed(HM_START_SERVICE, Utils.MINUTE*2);
    }

    private static int HM_START_SERVICE = 1;
    private static Handler workHandler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            if (msg.what == HM_START_SERVICE) {
                startService();
            }
        }
    };
}
