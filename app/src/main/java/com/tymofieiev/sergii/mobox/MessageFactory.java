package com.tymofieiev.sergii.mobox;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Sergii Tymofieiev on 26.01.2017.
 */


public class MessageFactory {
    private static ArrayList<MessageItem> parseJSON(String data) throws JSONException {
        ArrayList<MessageItem> parsedItemsList = new ArrayList<>();
        final JSONArray jsonReader;
        jsonReader = new JSONArray(data);
        for (int i = 0; i < jsonReader.length(); i++) {
            MessageItem message = new MessageItem();
            JSONObject element = jsonReader.getJSONObject(i);
            message.setDateStart(Utils.safeParseLong(Utils.safeGetJsonValue("create_at", element, ""),System.currentTimeMillis()));
            message.setMessageBody(Utils.safeGetJsonValue("text", element, ""));
            message.setMessageImageURL(Utils.safeGetJsonValue("image_url", element, ""));
            message.setMessageTitle(Utils.safeGetJsonValue("title", element, ""));
            parsedItemsList.add(message);
        }
        return parsedItemsList;
    }

    public static ArrayList<MessageItem> getAllMessages() throws JSONException {
        return parseJSON(getJSON());
    }


    public static String getJSON() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
                sb.append("{" +
                "    \"create_at\": " + String.valueOf(System.currentTimeMillis() - Utils.DAY) + ",\n" +
                "    \"title\": \"Fusce faucibus, eros sit amet fringilla pharetra, urna sem ornare ex, vitae lobortis purus metus sed est.\",\n" +
                "    \"text\": \"Donec elementum, enim in pharetra pharetra, magna lacus posuere dui, nec vestibulum dolor augue quis turpis. Nunc ornare pulvinar lacinia. Integer a tincidunt odio. Maecenas mollis mollis neque, vitae varius risus. Ut diam enim, feugiat in ultrices eget, congue vitae nulla. Quisque vulputate, ante vitae consectetur efficitur, massa ligula vestibulum nulla, at scelerisque eros ex vel ante. Donec scelerisque mauris lorem, non lacinia purus cursus non. Duis id urna et justo pulvinar semper. Pellentesque at pretium nisi. Ut sapien velit, lacinia vitae posuere sit amet, mattis eu justo.\\n Phasellus in sapien quis metus pretium pretium. Vestibulum ut facilisis eros. Vivamus odio nulla, mattis id ultrices mattis, bibendum eget nunc. Morbi interdum nulla id velit ultrices faucibus. Morbi tortor nibh, tincidunt nec sem ac, dignissim viverra est. Duis et rutrum mi. Sed bibendum nulla eget faucibus imperdiet. Donec gravida, justo varius volutpat finibus, ex velit vestibulum ligula, eu condimentum libero dolor semper nunc. Vestibulum at auctor metus, ut faucibus ex. Nunc eu consequat nibh. Pellentesque pulvinar in ex vitae suscipit. Aliquam eget auctor arcu, vitae gravida massa. Nunc tristique nisl non tellus convallis, quis posuere nisl malesuada. Nulla ullamcorper nisi elit, a porttitor mauris vulputate at. Nunc sed pellentesque libero. Donec hendrerit molestie libero quis pellentesque.\",\n" +
                "    \"image_url\": \"http://trinixy.ru/pics5/20170126/decoration_coins_02.jpg\"\n" +
                "  },");
        sb.append("{" +
                "    \"create_at\": " + String.valueOf(System.currentTimeMillis() - Utils.DAY*2) + ",\n" +
                "    \"title\": \"Fusce faucibus, eros sit amet fringilla pharetra, urna sem ornare ex, vitae lobortis purus metus sed est.\",\n" +
                "    \"text\": \"Donec elementum, enim in pharetra pharetra, magna lacus posuere dui, nec vestibulum dolor augue quis turpis. Nunc ornare pulvinar lacinia. Integer a tincidunt odio. Maecenas mollis mollis neque, vitae varius risus. Ut diam enim, feugiat in ultrices eget, congue vitae nulla. Quisque vulputate, ante vitae consectetur efficitur, massa ligula vestibulum nulla, at scelerisque eros ex vel ante. Donec scelerisque mauris lorem, non lacinia purus cursus non. Duis id urna et justo pulvinar semper. Pellentesque at pretium nisi. Ut sapien velit, lacinia vitae posuere sit amet, mattis eu justo.\\n Phasellus in sapien quis metus pretium pretium. Vestibulum ut facilisis eros. Vivamus odio nulla, mattis id ultrices mattis, bibendum eget nunc. Morbi interdum nulla id velit ultrices faucibus. Morbi tortor nibh, tincidunt nec sem ac, dignissim viverra est. Duis et rutrum mi. Sed bibendum nulla eget faucibus imperdiet. Donec gravida, justo varius volutpat finibus, ex velit vestibulum ligula, eu condimentum libero dolor semper nunc. Vestibulum at auctor metus, ut faucibus ex. Nunc eu consequat nibh. Pellentesque pulvinar in ex vitae suscipit. Aliquam eget auctor arcu, vitae gravida massa. Nunc tristique nisl non tellus convallis, quis posuere nisl malesuada. Nulla ullamcorper nisi elit, a porttitor mauris vulputate at. Nunc sed pellentesque libero. Donec hendrerit molestie libero quis pellentesque.\",\n" +
                "    \"image_url\": \"http://trinixy.ru/pics5/20170126/decoration_coins_04.jpg\"\n" +
                "  },");
        sb.append("{" +
                "    \"create_at\": " + String.valueOf(System.currentTimeMillis() - Utils.DAY*3) + ",\n" +
                "    \"title\": \"Fusce faucibus, eros sit amet fringilla pharetra, urna sem ornare ex, vitae lobortis purus metus sed est.\",\n" +
                "    \"text\": \"Donec elementum, enim in pharetra pharetra, magna lacus posuere dui, nec vestibulum dolor augue quis turpis. Nunc ornare pulvinar lacinia. Integer a tincidunt odio. Maecenas mollis mollis neque, vitae varius risus. Ut diam enim, feugiat in ultrices eget, congue vitae nulla. Quisque vulputate, ante vitae consectetur efficitur, massa ligula vestibulum nulla, at scelerisque eros ex vel ante. Donec scelerisque mauris lorem, non lacinia purus cursus non. Duis id urna et justo pulvinar semper. Pellentesque at pretium nisi. Ut sapien velit, lacinia vitae posuere sit amet, mattis eu justo.\\n Phasellus in sapien quis metus pretium pretium. Vestibulum ut facilisis eros. Vivamus odio nulla, mattis id ultrices mattis, bibendum eget nunc. Morbi interdum nulla id velit ultrices faucibus. Morbi tortor nibh, tincidunt nec sem ac, dignissim viverra est. Duis et rutrum mi. Sed bibendum nulla eget faucibus imperdiet. Donec gravida, justo varius volutpat finibus, ex velit vestibulum ligula, eu condimentum libero dolor semper nunc. Vestibulum at auctor metus, ut faucibus ex. Nunc eu consequat nibh. Pellentesque pulvinar in ex vitae suscipit. Aliquam eget auctor arcu, vitae gravida massa. Nunc tristique nisl non tellus convallis, quis posuere nisl malesuada. Nulla ullamcorper nisi elit, a porttitor mauris vulputate at. Nunc sed pellentesque libero. Donec hendrerit molestie libero quis pellentesque.\",\n" +
                "    \"image_url\": \"http://trinixy.ru/pics5/20170126/decoration_coins_07.jpg\"\n" +
                "  },");
        sb.append("{" +
                "    \"create_at\": " + String.valueOf(System.currentTimeMillis() - Utils.DAY*4) + ",\n" +
                "    \"title\": \"Fusce faucibus, eros sit amet fringilla pharetra, urna sem ornare ex, vitae lobortis purus metus sed est.\",\n" +
                "    \"text\": \"Donec elementum, enim in pharetra pharetra, magna lacus posuere dui, nec vestibulum dolor augue quis turpis. Nunc ornare pulvinar lacinia. Integer a tincidunt odio. Maecenas mollis mollis neque, vitae varius risus. Ut diam enim, feugiat in ultrices eget, congue vitae nulla. Quisque vulputate, ante vitae consectetur efficitur, massa ligula vestibulum nulla, at scelerisque eros ex vel ante. Donec scelerisque mauris lorem, non lacinia purus cursus non. Duis id urna et justo pulvinar semper. Pellentesque at pretium nisi. Ut sapien velit, lacinia vitae posuere sit amet, mattis eu justo.\\n Phasellus in sapien quis metus pretium pretium. Vestibulum ut facilisis eros. Vivamus odio nulla, mattis id ultrices mattis, bibendum eget nunc. Morbi interdum nulla id velit ultrices faucibus. Morbi tortor nibh, tincidunt nec sem ac, dignissim viverra est. Duis et rutrum mi. Sed bibendum nulla eget faucibus imperdiet. Donec gravida, justo varius volutpat finibus, ex velit vestibulum ligula, eu condimentum libero dolor semper nunc. Vestibulum at auctor metus, ut faucibus ex. Nunc eu consequat nibh. Pellentesque pulvinar in ex vitae suscipit. Aliquam eget auctor arcu, vitae gravida massa. Nunc tristique nisl non tellus convallis, quis posuere nisl malesuada. Nulla ullamcorper nisi elit, a porttitor mauris vulputate at. Nunc sed pellentesque libero. Donec hendrerit molestie libero quis pellentesque.\",\n" +
                "    \"image_url\": \"http://trinixy.ru/pics5/20170126/decoration_coins_10.jpg\"\n" +
                "  },");
        sb.append("  {\n" +
                "    \"create_at\": " + String.valueOf(System.currentTimeMillis() - Utils.DAY * 5) + ",\n" +
                "    \"title\": \"Proin venenatis accumsan tempor. Nulla posuere congue mauris vel accumsan.\",\n" +
                "    \"text\": \"In vestibulum bibendum neque. Etiam malesuada, erat ultricies ultrices consequat, lectus urna eleifend velit, non mattis odio eros sit amet sapien. Maecenas nunc ante, posuere nec iaculis a, ultrices a quam. Suspendisse a augue vitae erat dictum aliquet. Etiam dictum eget quam ut interdum. Nulla at justo eros. Vivamus viverra aliquet metus, in feugiat quam fermentum a. Vestibulum posuere eget quam vitae ultricies. Cras sed lacus vulputate, dictum velit non, condimentum urna.\\n Ut vehicula scelerisque odio a volutpat. Nulla non tincidunt felis, eget malesuada metus. Aliquam tempor felis sit amet rhoncus venenatis. Nulla urna quam, tincidunt ut ante nec, consectetur tincidunt ex. In vel sodales risus, et aliquam neque. Nunc venenatis, turpis a mattis dictum, lectus eros mattis dolor, quis elementum justo est in quam. Nunc at malesuada orci. Aliquam vestibulum, lorem eu luctus ornare, nulla odio eleifend leo, id ullamcorper urna urna eget velit. Ut non dui eget ante suscipit lacinia eu sit amet velit. Morbi turpis est, sollicitudin at venenatis non, gravida a sapien. Aliquam viverra mi eget arcu laoreet iaculis. Morbi at mollis risus, eu aliquam tellus.\",\n" +
                "    \"image_url\": \"http://trinixy.ru/pics5/20170126/decoration_coins_12.jpg\"\n" +
                "  }]");


        return sb.toString();
    }
}

