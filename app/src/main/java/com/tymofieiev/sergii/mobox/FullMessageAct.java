package com.tymofieiev.sergii.mobox;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by Sergii Tymofieiev on 26.01.2017.
 */

public class FullMessageAct extends CommonAct {
    MessageItem messageItem;
    private ImageView mImageViewCover;
    private TextView mTextViewCover1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.full_message_act);
        Intent intent = getIntent();
        messageItem = intent.getParcelableExtra(Constants.ACTION_EXTRA_0);

        mImageViewCover = (ImageView) findViewById(R.id.dsi_cover);
        mTextViewCover1 = (TextView) findViewById(R.id.dsi_header_1);

    }

    @Override
    protected void onResume() {
        super.onResume();
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();
        if (messageItem != null) {
            ImageLoader.getInstance().displayImage(messageItem.getMessageImageURL(), mImageViewCover, options, new AnimateFirstDisplayListener());
            mTextViewCover1.setText(messageItem.getMessageBody());
        }
    }
}
