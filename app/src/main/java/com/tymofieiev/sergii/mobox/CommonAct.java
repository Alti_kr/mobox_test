package com.tymofieiev.sergii.mobox;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.app.FragmentActivity;

/**
 * Created by Sergii Tymofieiev on 27.01.2017.
 */

public class CommonAct extends FragmentActivity {
    private MessageDialog dialog;
    protected BroadcastReceiver broadcastReceiver;

    @Override
    protected void onResume() {
        super.onResume();
        if (broadcastReceiver == null) {
            broadcastReceiver = makeBroadcastReceiver();
        }
        registerReceiver(broadcastReceiver, new IntentFilter(Constants.ACTION_SHOW_DIALOG));

    }

    private BroadcastReceiver makeBroadcastReceiver() {
        return new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (Constants.ACTION_SHOW_DIALOG.equals(intent.getAction())) {
                    showMessage();
                }
            }
        };

    }

    private void showMessage() {
        if (dialog != null && dialog.isResumed()) {
            dialog.dismiss();
        }

        dialog = new MessageDialog();
        dialog.show(getSupportFragmentManager(), "dialog");
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (broadcastReceiver != null) {
            unregisterReceiver(broadcastReceiver);
        }
        broadcastReceiver = null;
    }


}
