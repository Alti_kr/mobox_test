package com.tymofieiev.sergii.mobox;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Sergii Tymofieiev on 26.01.2017.
 */


public class MessageItem implements Parcelable {


    private long dateStart;
    private String messageTitle, messageImageURL, messageBody;

    public MessageItem() {
    }
    public void setDateStart(long dateStart){
        this.dateStart = dateStart;
    }
    public void setMessageTitle(String title){
        this.messageTitle = title;
    }
    public void setMessageImageURL(String messageImageURL){
        this.messageImageURL = messageImageURL;
    }

    public void setMessageBody(String messageBody) {
        this.messageBody = messageBody;
    }

    public String getMessageTitle() {
        return messageTitle;
    }

    public String getMessageBody() {
        return messageBody;
    }

    public String getMessageImageURL() {
        return messageImageURL;
    }

    public long getDateStart() {
        return dateStart;
    }

    @Override
    public int describeContents() {
        return 0;
    }
    public static final Parcelable.Creator<MessageItem> CREATOR
            = new Parcelable.Creator<MessageItem>() {
        public MessageItem createFromParcel(Parcel in) {
            return new MessageItem(in);
        }

        public MessageItem[] newArray(int size) {
            return new MessageItem[size];
        }
    };
    @Override
    public void writeToParcel(Parcel out, int i) {
        out.writeString(messageTitle);
        out.writeString(messageBody);
        out.writeString(messageImageURL);
        out.writeLong(dateStart);
    }

    public MessageItem(Parcel in) {
        this.messageTitle = in.readString();
        this.messageBody = in.readString();
        this.messageImageURL = in.readString();
        this.dateStart = in.readLong();
    }
}



