package com.tymofieiev.sergii.mobox;


import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;


import org.json.JSONException;

import java.util.ArrayList;


/**
 * Created by Sergii Tymofieiev on 26.01.2017.
 */

public class MessageAct extends CommonAct {
    protected RecyclerView mRecyclerView;
    private ArrayList<MessageItem> itemsList;
    SwipeToAction swipeToAction;
    private MessageListAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ImageUtils.initImageLoader(this);
        setContentView(R.layout.message_act);
        itemsList = new ArrayList<>();
        try {
            itemsList = MessageFactory.getAllMessages();
        } catch (JSONException e) {
            Log.d("My_log", e.toString());
        }
        LinearLayoutManager expenseLayoutManager = new CustomLinearLayoutManager(this);
        expenseLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);//
        mRecyclerView = (RecyclerView) findViewById(R.id.dealer_frg_news_list);
        adapter = new MessageListAdapter();

        mRecyclerView.setLayoutManager(expenseLayoutManager);
        mRecyclerView.setAdapter(adapter);
        swipeToAction = new SwipeToAction(mRecyclerView, new SwipeToAction.SwipeListener<MessageItem>() {
            @Override
            public boolean swipeLeft(final MessageItem itemData) {
                final int pos = removeMessageFromView(itemData);
                displaySnackbar(null, Utils.getStringById(MessageAct.this, R.string.btn_cancel_cup), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        addMessageToView(pos, itemData);
                    }
                }, new Snackbar.Callback() {
                    @Override
                    public void onDismissed(Snackbar snackbar, int event) {
                    }
                });
                return true;
            }

            @Override
            public boolean swipeRight(MessageItem itemData) {
                return true;
            }

            @Override
            public void onClick(SwipeToAction.ViewHolder viewHolder, MessageItem itemData) {
                showFullMessage(viewHolder, itemData);

            }

            @Override
            public void onLongClick(MessageItem itemData) {
                Log.d("My_log", "onLongClick");
            }
        });
    }

    private void showFullMessage(SwipeToAction.ViewHolder viewHolder, MessageItem messageItem) {
        Intent intent = new Intent(this, FullMessageAct.class);
        intent.putExtra(Constants.ACTION_EXTRA_0, messageItem);
        ImageView imageView = ((MessageViewHolder) viewHolder).mImageViewCover;
        ActivityOptions options = ActivityOptions
                .makeSceneTransitionAnimation(this, imageView, "profile");
        startActivity(intent, options.toBundle());
    }


    private int removeMessageFromView(MessageItem itemData) {
        int pos = itemsList.indexOf(itemData);
        itemsList.remove(itemData);
        adapter.notifyItemRemoved(pos);
        return pos;

    }

    private void addMessageToView(int pos, MessageItem itemData) {
        itemsList.add(pos, itemData);
        adapter.notifyItemInserted(pos);
    }

    private void displaySnackbar(String text, String actionName, View.OnClickListener action, Snackbar.Callback callback) {
        Snackbar snack = Snackbar.make(findViewById(android.R.id.content), text, Snackbar.LENGTH_LONG)
                .setAction(actionName, action)
                .setCallback(callback);


        View v = snack.getView();
        v.setBackgroundColor(ContextCompat.getColor(MessageAct.this, R.color.grey));
        ((TextView) v.findViewById(android.support.design.R.id.snackbar_text)).setTextColor(ContextCompat.getColor(MessageAct.this, R.color.orange));
        ((TextView) v.findViewById(android.support.design.R.id.snackbar_action)).setTextColor(ContextCompat.getColor(MessageAct.this, R.color.dark_background));

        snack.show();
    }


    public class MessageListAdapter extends RecyclerView.Adapter<MessageViewHolder> {
        private DisplayImageOptions options;
        ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();

        public MessageListAdapter() {
            options = new DisplayImageOptions.Builder()
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .build();

        }

        @Override
        public MessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.message_item_cover, parent, false);

            return new MessageViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final MessageViewHolder holder, int position) {
            MessageItem tMessage = itemsList.get(position);
            holder.mMessage = tMessage;
            ImageLoader.getInstance().displayImage(holder.mMessage.getMessageImageURL(), holder.mImageViewCover, options, animateFirstListener);
            holder.mTextViewCover0.setText(tMessage.getMessageTitle());
            holder.mTextViewCoverDate0.setText(Utils.getFormattedDateAsString(Utils.DateFormatCustom.ddMMyyyy, tMessage.getDateStart()));
            holder.mTextViewCover1.setText(tMessage.getMessageBody());
        }

        @Override
        public int getItemCount() {
            return itemsList.size();
        }

    }


    protected class MessageViewHolder extends SwipeToAction.ViewHolder {


        protected ImageView mImageViewCover;
        protected TextView mTextViewCover0, mTextViewCover1;
        protected TextView mTextViewCoverDate0;
        private MessageItem mMessage;


        public MessageViewHolder(View itemView) {
            super(itemView);
            init(itemView);

            mImageViewCover = (ImageView) itemView.findViewById(R.id.dsi_cover);
            mTextViewCover0 = (TextView) itemView.findViewById(R.id.dsi_header_0);
            mTextViewCoverDate0 = (TextView) itemView.findViewById(R.id.dsi_header_date);
            mTextViewCover1 = (TextView) itemView.findViewById(R.id.dsi_header_1);
        }

        public MessageItem getItemData() {
            return mMessage;
        }

        @Override
        public boolean isReadyForSwipe() {
            return true;
        }


    }

}

