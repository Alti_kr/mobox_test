package com.tymofieiev.sergii.mobox;

import android.app.IntentService;
import android.content.Intent;

/**
 * Created by Sergii Tymofieiev on 27.01.2017.
 */

public class MessageService extends IntentService {
    public MessageService() {
        super(MessageService.class.getName());
    }

    public MessageService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            if (intent.getBooleanExtra(Constants.SERVICE_ACTION_START, false)) {
                doSomething();
            }
        }
    }

    private void doSomething() {
        sendBroadcast(new Intent(Constants.ACTION_SHOW_DIALOG));
    }
}
