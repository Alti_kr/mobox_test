package com.tymofieiev.sergii.mobox;

/**
 * Created by Sergii Tymofieiev on 26.01.2017.
 */

public class Constants {

    public final static String ACTION_SHOW_DIALOG = "com.tymofieiev.sergii.mobox.ACTION_SHOW_DIALOG";
    public final static String ACTION_EXTRA_0 = "com.tymofieiev.sergii.mobox.ACTION_EXTRA_0";
    public final static String SERVICE_ACTION_START = "com.tymofieiev.sergii.mobox.SERVICE_ACTION_START";;

}
